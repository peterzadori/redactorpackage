<?php

namespace Packages\RedactorPackage;

use movi\DI\CompilerExtension;
use movi\Packages\IPackage;
use movi\Packages\Providers\IAssetsProvider;
use movi\Packages\Providers\IConfigProvider;

class RedactorPackage extends CompilerExtension implements IPackage, IConfigProvider, IAssetsProvider
{

    public function getConfigFiles()
    {
        return array(
            __DIR__ . '/Resources/config/package.neon'
        );
    }


    public function getAssets()
    {
        return [
            'css' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/redactor/redactor.css'
                    ]
                ]
            ],
            'js' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/redactor/redactor.js',
                        __DIR__ . '/Resources/assets/redactor/lang/cs.js',
                        __DIR__ . '/Resources/assets/redactor.table.js',
                        __DIR__ . '/Resources/assets/redactor.ext.js'
                    ]
                ]
            ]
        ];
    }

} 
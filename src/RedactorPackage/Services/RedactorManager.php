<?php

namespace Packages\RedactorPackage\Services;

use Nette\Http\FileUpload;

class RedactorManager
{

	/** @var \Packages\RedactorPackage\Services\RedactorUploader */
	private $redactorUploader;


	public function __construct(RedactorUploader $redactorUploader)
	{
		$this->redactorUploader = $redactorUploader;
	}


	public function uploadImage(FileUpload $fileUpload)
	{
		return $this->redactorUploader->upload($fileUpload, RedactorUploader::IMAGE);
	}


	public function uploadFile(FileUpload $fileUpload)
	{
		return $this->redactorUploader->upload($fileUpload, RedactorUploader::FILE);
	}

} 
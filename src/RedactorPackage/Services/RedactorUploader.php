<?php

namespace Packages\RedactorPackage\Services;

use movi\Files\File;
use movi\Files\FilesManager;
use Nette\Http\FileUpload;
use Nette\Http\Request;
use Nette\Utils\Strings;

class RedactorUploader
{

	const FILE = 'file',
		IMAGE = 'image';

	/** @var \movi\Files\FilesManager */
	private $filesManager;

	/** @var \Nette\Http\Request */
	private $request;

	/** @var string */
	private $wwwDir;


	public function __construct(FilesManager $filesManager, Request $request, $wwwDir)
	{
		$this->filesManager = $filesManager;
		$this->request = $request;
		$this->wwwDir = $wwwDir;
	}


	public function upload(FileUpload $fileUpload, $type = self::IMAGE)
	{
		$file = new File($fileUpload->name, $fileUpload->contents);
		$file->setNamespace('redactor/' . $type);

		$this->filesManager->write($file);

		return $this->request->url->basePath . Strings::substring($file->getPath(), Strings::length($this->wwwDir) + 1);
	}

} 
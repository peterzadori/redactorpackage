(function($) {
    $.nette.ext({
        load: function() {
            $('.redactor').each(function() {
                var options = $(this).data('options');

                options.focus = true;
                options.plugins = ['table'];

                if ($(this).data('initialized') === undefined) {
                    $(this).data('initialized', true);
                    $(this).redactor(options);
                }
            });
        }
    });
})(jQuery);
<?php

namespace Packages\RedactorPackage\Forms\Controls;

use movi\Files\FilesManager;
use movi\InvalidArgumentException;
use Packages\RedactorPackage\Services\RedactorManager;
use Nette\Application\UI\ISignalReceiver;
use Nette\Application\UI\Presenter;
use Nette\Forms\Controls\TextArea;
use Nette\Utils\Strings;
use Nextras\Forms\Controls\Fragments\ComponentControlTrait;

class RedactorControl extends TextArea implements ISignalReceiver
{

	use ComponentControlTrait;


	private $properties = [
		'air' => NULL,
		'airButtons' => NULL,
		'allowedTags' => NULL,
		'autoresize' => NULL,
		'autosave' => NULL,
		'autosaveCallback' => NULL,
		'autosaveInterval' => NULL,
		'boldTag' => NULL,
		'buttons' => NULL,
		'buttonsHideOnMobile' => NULL,
		'buttonSource' => true,
		'cleanFontTag' => NULL,
		'cleanSpaces' => NULL,
		'cleanup' => NULL,
		'clipboardUploadUrl' => NULL,
		'convertDivs' => NULL,
		'convertImageLinks' => NULL,
		'convertLinks' => NULL,
		'convertVideoLinks' => NULL,
		'css' => NULL,
		'deniedTags' => NULL,
		'direction' => NULL,
		'dragUpload' => NULL,
		'fileUpload' => NULL,
		'fileUploadCallback' => NULL,
		'fileUploadError' => NULL,
		'fileUploadParam' => NULL,
		'focus' => NULL,
        'formatting' => NULL,
		'formattingPre' => NULL,
		'formattingTags' => NULL,
        'formattingAdd' => NULL,
		'fullpage' => NULL,
		'iframe' => NULL,
		'imageFloatMargin' => NULL,
		'imageGetJson' => NULL,
		'imageTabLink' => NULL,
		'imageUpload' => NULL,
		'imageUploadCallback' => NULL,
		'imageUploadErrorCallback' => NULL,
		'imageUploadParam' => NULL,
		'italicTag' => NULL,
		'lang' => NULL,
		'linebreaks' => NULL,
		'linkNofollow' => NULL,
		'linkProtocol' => NULL,
		'linkSize' => NULL,
		'maxHeight' => NULL,
		'maxWidth' => NULL,
		'mobile' => NULL,
		'modalOverlay' => NULL,
		'observeImages' => NULL,
		'observeLinks' => NULL,
		'paragraphy' => NULL,
		'pastePlainText' => NULL,
		'phpTags' => NULL,
		'placeholder' => NULL,
		'predefinedLinks' => NULL,
		'removeEmptyTags' => NULL,
		'replaceDivs' => NULL,
		's3' => NULL,
		'shortcuts' => NULL,
		'shortcutsAdd' => NULL,
		'tabFocus' => NULL,
		'tabindex' => NULL,
		'tabSpaces' => NULL,
		'tidyHtml' => NULL,
		'toolbar' => NULL,
		'toolbarExternal' => NULL,
		'toolbarFixed' => NULL,
		'toolbarFixedBox' => NULL,
		'toolbarFixedTarget' => NULL,
		'toolbarFixedTopOffset' => NULL,
		'toolbarOverflow' => NULL,
		'uploadFields' => NULL,
		'visual' => NULL,
		'wym' => NULL
	];

    static public $overrideProperties = [];

	/** @var RedactorManager */
	private $redactorManager;

	private $uploadsAllowed = true;


	public function __construct($label = NULL)
	{
		parent::__construct($label);

		$this->controlPrototype->class[] = 'redactor';
		$this->monitor('Nette\Application\UI\Presenter');
	}


	public function attached($parent)
	{
		parent::attached($parent);

		if ($parent instanceof Presenter) {
			$this->redactorManager = $parent->context->getByType('Packages\RedactorPackage\Services\RedactorManager');

			if ($this->uploadsAllowed) {
				$this->setProperty('fileUpload', $this->link('uploadFile!'));
				$this->setProperty('imageUpload', $this->link('uploadImage!'));
			}
		}
	}


	public function handleUploadFile()
	{
		$fileUpload = $this->getPresenter()->request->files['file'];
		$file = $this->redactorManager->uploadFile($fileUpload);

		$this->getPresenter()->sendJson(['filelink' => $file]);
	}


	public function handleUploadImage()
	{
		$fileUpload = $this->getPresenter()->request->files['file'];
		$file = $this->redactorManager->uploadImage($fileUpload);

		$this->getPresenter()->sendJson(['filelink' => $file]);
	}


	public function getControlPrototype()
	{
		$control = parent::getControlPrototype();

		$properties = $this->getProperties();

		$control->data(array(
			'options' => $properties
		));

		return $control;
	}


	/**
	 * @param $option
	 * @param $value
	 * @return $this|\Nette\Forms\Controls\BaseControl
	 * @throws \movi\InvalidArgumentException
	 */
	public function setProperty($option, $value)
	{
		if (!array_key_exists($option, $this->properties)) {
			throw new InvalidArgumentException("Unknown option: '$option'");
		}

		$this->properties[$option] = $value;

		return $this;
	}


	/**
	 * @param array $options
	 * @return $this
	 * @throws \movi\InvalidArgumentException
	 */
	public function setProperties(array $options)
	{
		foreach ($options as $option => $value)
		{
			$this->setProperty($option, $value);
		}

		return $this;
	}


    public function getProperties()
    {
        $merged = array_merge($this->properties, self::$overrideProperties);

        return array_filter($merged, function($value) {
            return $value !== NULL;
        });
    }


	/**
	 * @return $this
	 */
	public function disableUploads()
	{
		$this->uploadsAllowed = false;

		return $this;
	}

} 